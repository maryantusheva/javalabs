package com.antusheva.lib;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        Car car = new Car();
        car.start();
        System.out.println(" -Current car state:" + car.getState());
    }
}
